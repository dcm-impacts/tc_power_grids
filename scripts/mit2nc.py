
import pathlib
import re
import zipfile

import cartopy.io.shapereader as shpreader
from climada.hazard import TCTracks
from climada.hazard.tc_tracks import _read_file_emanuel
import geopandas as gpd
import numpy as np
import shapely.geometry
import sys


MIN_WIND_KN = 64
"Minimum wind speeds for Cat. 1 on Saffir-Simpson scale"""

AP_BASIN_BOUNDS = (-180, 0, 0, 90)
"""Geographical bounds of the North Atlantic/West Pacific basin"""

WP_BOUNDARY = np.array([(-83.0, 9.0), (-96.0, 18.0)])
WP_BOUNDARY_TANG = WP_BOUNDARY[1, :] - WP_BOUNDARY[0, :]
WP_NORMAL = np.array([(0.0, -1.0), (1.0, 0.0)]).dot(WP_BOUNDARY_TANG)
"""The straight line that forms the boundary between WP and NA basin

This definition uses the tangent and normal (pointing towards WP) and
should be useful when applied to genesis locations.
"""

SHP_RESOLUTION = "50m"
"""Resolution of Texas coastline for defining the landfall area"""

HIST_PERIOD = (1985, 2014)
"""Last 30 years of historical ISIMIP3a period"""


def within_wp(lon, lat):
    coords = np.stack([lon, lat], axis=-1)
    return (
        (coords - WP_BOUNDARY[0, None, :]) * WP_NORMAL[None, :]
    ).sum(axis=-1) >= 0


def within_bounds(lon, lat, bounds):
    return (
        (lon >= bounds[0])
        & (lon <= bounds[2])
        & (lat >= bounds[1])
        & (lat <= bounds[3])
    )


def multi_ds_to_arr(l_ds, v):
    values = [ds[v].values for ds in l_ds]
    max_shape = np.array([arr.shape for arr in values]).max(axis=0)
    return np.stack([
        np.pad(
            arr,
            [(0, ms - s) for ms, s in zip(max_shape, arr.shape)],
            mode="constant",
            constant_values=np.nan,
        )
        for arr in values
    ], axis=0)


def apply_mask_multi(l_arr, mask):
    return [
        (
            [v for v, m in zip(arr, mask) if m]
            if isinstance(arr, list)
            else arr[mask]
        ) for arr in l_arr
    ]


def load_tx_lf_geoms():
    # get texas border shape, apply 1 degree padding
    adm1_df = gpd.read_file(shpreader.natural_earth(
        resolution=SHP_RESOLUTION,
        category='cultural',
        name='admin_1_states_provinces_lakes'))
    texas_geom = adm1_df[adm1_df["iso_3166_2"] == "US-TX"].geometry.unary_union.buffer(1)

    # get coastline shape, apply 1 degree padding, take intersection as the "landfall region"
    lf_geom = gpd.read_file(
        shpreader.natural_earth(
            resolution=SHP_RESOLUTION,
            category='physical',
            name='coastline',
        )
    ).geometry.unary_union.intersection(
        shapely.geometry.box(*texas_geom.bounds)
    ).buffer(1).intersection(texas_geom)

    return lf_geom, lf_geom.buffer(4)


def filter_tx(tracks):
    lon = multi_ds_to_arr(tracks.data, "lon")
    lat = multi_ds_to_arr(tracks.data, "lat")
    lon, lat, tracks.data = apply_mask_multi(
        [lon, lat, tracks.data],
        within_bounds(lon, lat, AP_BASIN_BOUNDS).any(axis=-1),
    )
    lon, lat, tracks.data = apply_mask_multi(
        [lon, lat, tracks.data],
        ~within_wp(lon[:, 0], lat[:, 0]),
    )
    winds = multi_ds_to_arr(tracks.data, "max_sustained_wind")
    lon, lat, winds, tracks.data = apply_mask_multi(
        [lon, lat, winds, tracks.data],
        (winds >= MIN_WIND_KN).any(axis=-1),
    )

    lf_geom, lfext_geom = load_tx_lf_geoms()

    # require that some track step be included in the landfall region
    points = [gpd.points_from_xy(x, y) for x, y in zip(lon, lat)]
    lon, lat, winds, points, tracks.data = apply_mask_multi(
        [lon, lat, winds, points, tracks.data],
        np.array([p.within(lf_geom) for p in points], dtype=bool).any(axis=-1),
    )

    # require that winds exceed threshold within extended landfall region
    mask = np.array([p.within(lfext_geom) for p in points], dtype=bool)
    winds[~mask] = np.nan
    [tracks.data] = apply_mask_multi([tracks.data], (winds >= MIN_WIND_KN).any(axis=-1))

    return tracks


def read_mat(fp, name):
    m = re.match(r"Potsdam_cmip6_GB_([a-z0-9]+)_([a-z0-9]+)cal.zip", name)
    model, flavor = m.group(1), m.group(2)

    tracks = TCTracks()
    tracks.data = _read_file_emanuel(fp)

    # include only the last 30 years in the data set
    years = np.array([tr.time.dt.year[0] for tr in tracks.data])
    max_year = years.max()
    period = (max_year - 29, max_year)
    mask = (years >= period[0]) & (years <= period[1])
    tracks.data = [tr for tr, m in zip(tracks.data, mask) if m]

    name = f"{model}_{flavor}_{period[0]}-{period[1]}"
    return name, tracks


def read_ibtracs(period):
    tracks = TCTracks.from_ibtracs_netcdf(year_range=period, genesis_basin="NA")
    return f"ibtracs_{period[0]}-{period[1]}", tracks


def main(path):
    if path is None:
        name, tracks = read_ibtracs(HIST_PERIOD)
    else:
        path = pathlib.Path(path)
        with zipfile.ZipFile(path) as zipfp:
            filenames = zipfp.namelist()
            [mat_file] = [f for f in filenames if f.endswith(".mat")]
            with zipfp.open(mat_file) as fp:
                name, tracks = read_mat(fp, path.name)
    outpath = pathlib.Path(".") / "cache" / name
    tracks = filter_tx(tracks)
    outpath.mkdir(parents=True, exist_ok=True)
    tracks.write_netcdf(outpath)


if __name__ == "__main__":
    main(None if len(sys.argv) < 2 else sys.argv[1])
