import pathlib

import cartopy.crs as ccrs
import cartopy.feature as cfeature
import cartopy.io.shapereader as shpreader
import matplotlib.pyplot as plt
import xarray as xr

SHP_RESOLUTION = "50m"
TRACK_DATA_DIR = pathlib.Path(".")

def main():
    fig = plt.figure(figsize=(10, 4))
    ax = plt.axes(projection=ccrs.PlateCarree())

    # add base map
    linewidth = 0.5
    features = [
        (cfeature.OCEAN, "face", "#EDFBFF"),
        (cfeature.LAND, "face", "#DDDDDD"),
        (cfeature.STATES, "white", "#DDDDDD"),
    ]
    for feat, edgecolor, facecolor in features:
        ax.add_feature(
            feat.with_scale(SHP_RESOLUTION),
            linewidth=linewidth,
            edgecolor=edgecolor, facecolor=facecolor)

    # to highlight Texas, manually extract from the Natural Earth dataset
    reader = shpreader.Reader(shpreader.natural_earth(
        resolution=SHP_RESOLUTION,
        category='cultural',
        name='admin_1_states_provinces_lakes'))
    tx_geom = [s for s in reader.records() if s.attributes['iso_3166_2'] == "US-TX"][0].geometry
    ax.add_geometries(
        [tx_geom], ccrs.PlateCarree(), edgecolor="#AAAAAA", facecolor="white", linewidth=0.5)

    # unfortunately, cartopy's inline gridline labels don't look very nice
    xlocs = [-105.]
    ylocs = [25.]
    ax.gridlines(ylocs=ylocs, xlocs=xlocs)
    for loc in xlocs:
        ax.text(
            loc, 26, f"{abs(loc):.0f}°{'W' if loc < 0 else 'E'}",
            rotation="vertical",
            horizontalalignment="right",
            verticalalignment="bottom",
        )
    for loc in ylocs:
        ax.text(
            -108, loc, f"{abs(loc):.0f}°{'S' if loc < 0 else 'N'}",
            horizontalalignment="center",
            verticalalignment="bottom",
        )

    # ========== add a plot of the grid topology here ==========

    # plot the tracks over
    tracks = [xr.open_dataset(path) for path in TRACK_DATA_DIR.glob("tc_track-*-orig.nc")]
    for track in tracks:
        category = "TS" if track.category == 0 else track.category
        name = f"{track.name[0].upper()}{track.name[1:].lower()} ({category})"
        ax.plot(track.lon, track.lat, label=name)

    ax.set_extent([-110, -78, 24, 37], ccrs.PlateCarree())
    ax.legend(loc="upper right")

    fig.tight_layout()
    plt.show()

if __name__ == "__main__":
    main()
