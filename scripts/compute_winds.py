
import argparse
import pathlib

from climada.hazard import TCTracks, TropCyclone, Centroids
from climada_petals.hazard import TCForecast
import numpy as np
import xarray as xr


"""Output files (NetCDFs containing wind footprints) are placed in this directory"""
OUTPUT_DIR = pathlib.Path("./output/")

"""Geographical bounds (around Texas) considered for wind footprint calculation"""
TX_BOUNDS = (-107, 25, -93, 37)

"""Spatial resolution at which to compute the wind footprints"""
RESOLUTION_DEG = 0.1


def setup_centroids():
    """Set up a regular grid in lat/lon coordinates on which to compute wind footprints

    The grid resolution and extent are hardcoded in RESOLUTION_DEG and TX_BOUNDS.

    Returns
    -------
    climada.hazard.Centroids
    """
    centroids = Centroids.from_pnt_bounds(TX_BOUNDS, res=RESOLUTION_DEG)
    centroids.set_dist_coast(precomputed=True)
    centroids.check()
    centroids.set_meta_to_lat_lon()
    return centroids


def get_track_name(track):
    """Assign a TC event name to a given track dataset

    If the track is from a forecast, the name and ID are included, as well as the ensemble number
    if applicable. Otherwise the track's `sid` attribute is used which is particularly meaningful
    for best track data from IBTrACS.

    Parameters
    ----------
    track : xarray.Dataset
        A dataset describing either a historical (best track) or forecasted TC track.

    Returns
    -------
    str
    """
    return track.sid if "is_ensemble" not in track.attrs else (
        f"{track.name}_{track.sid[:10]}" + (
            f"_ens{int(track.ensemble_number):02d}" if track.is_ensemble else ""
        )
    )


def read_tracks(time_step_min, ibtracs_id=None, fc_path=None, fc_name=None, mit_path=None):
    """Obtain TC track data from IBTrACS or from a given forecast file

    Parameters
    ----------
    time_step_min : float
        The TC track data is interpolated to this temporal resolution.
    ibtracs_id : str, optional
        If given, extract the specified storm from IBTrACS.
    fc_path : Path or str, optional
        If no IBTrACS ID is specified, all tracks are taken from the given forecast file.
    fc_name : str, optional
        The name of the track to extract from the forecast file. Note that the initial time of the
        forecast must be chosen in such a way that the official naming of the storm was already in
        effect.
    mit_path : Path or str, optional
        If no IBTrACS ID or forecast data is specified, all tracks are taken from files containing
        a selection of simulated tracks (MIT) from ISIMIP3. This is the path to the directory
        containing the data as individual NetCDF files.

    Returns
    -------
    path : Path
        The output path where the tracks have been stored in their original and upsampled
        temporal resolution
    tracks : TCTracks
        The TC track data.
    """
    path = OUTPUT_DIR / "historical"
    if ibtracs_id is not None:
        tracks = TCTracks.from_ibtracs_netcdf(storm_id=ibtracs_id)
    elif fc_path is not None:
        tracks = TCForecast.read_cxml(fc_path)
        tracks.data = [
            tr for tr in tracks.data
            if tr.name == fc_name
        ]
        path = OUTPUT_DIR / "forecast"
    else:
        tracks = TCTracks.from_netcdf(mit_path)
        # when writing '<U2' and reading in again, xarray reads as dtype 'object'. undo this:
        for track in tracks.data:
            track['basin'] = track['basin'].astype('<U2')
        path = OUTPUT_DIR / "isimip3_mit" / f"{mit_path.name}"
    path.mkdir(parents=True, exist_ok=True)
    for tr in tracks.data:
        store_track(path, tr,  "orig")
    tracks.equal_timestep(time_step_h=time_step_min / 60)
    for tr in tracks.data:
        store_track(path, tr,  time_step_min)
    return path, tracks


def store_track(path, track, time_step_min):
    """Store a single TC track dataset to a NetCDF file for further analysis

    Parameters
    ----------
    path : Path
        Path to output directory.
    track : xr.Dataset
        A dataset describing either a historical (best track) or forecasted TC track.
    time_step_min : float or "orig"
        The resolution of the temporal interpolation (or "orig" if no interpolation has been
        applied) will be stored as part of the file name.
    """
    track.attrs["orig_event_flag"] = int(track.orig_event_flag)

    run_datetime = None
    if "run_datetime" in track.attrs:
        track.attrs["is_ensemble"] = int(track.is_ensemble)
        run_datetime = track.attrs["run_datetime"]
        del track.attrs["run_datetime"]

    suffix = "orig" if time_step_min == "orig" else f"{int(time_step_min)}min"
    track.to_netcdf(path=path / f"tc_track-{get_track_name(track)}-{suffix}.nc")

    if run_datetime is not None:
        track.attrs["run_datetime"] = run_datetime


def compute_wind(track, centroids):
    """Compute dynamic TC wind footprints on a given grid from a TC track dataset

    Parameters
    ----------
    track : xr.Dataset
        A dataset describing either a historical (best track) or forecasted TC track.
    centroids : climada.hazard.Centroids
        Object describing the spatial locations at which to compute wind footprints.

    Returns
    -------
    wind : np.ndarray
        A three-dimensional array. The first dimension corresponds to the time dependency, the
        second to the spatial extent (one entry for each centroid), and the last dimension stores
        the two vectorial components of the wind speed.
    """
    tr_sel = TCTracks()
    tr_sel.append(track)
    tc_tmp = TropCyclone.from_tracks(tr_sel, centroids, store_windfields=True)
    # no intensity threshold is applied when using the "windfields" attribute!
    wind = tc_tmp.windfields[0].toarray()
    # convert 1-min sustained winds to 10-min sustained winds
    wind = 0.88 * wind.reshape(wind.shape[0], -1, 2)
    return wind


def store_wind(base_dir, track, centroids, wind, time_step_min, vectors=False):
    """Store TC wind footprints in a NetCDF file

    Parameters
    ----------
    base_dir : Path
        Path to the output directory.
    track : xr.Dataset
        A dataset describing either a historical (best track) or forecasted TC track.
    centroids : climada.hazard.Centroids
        Object describing the spatial locations at which to compute wind footprints.
    wind : np.ndarray
        A three-dimensional array. The first dimension corresponds to the time dependency, the
        second to the spatial extent (one entry for each centroid), and the last dimension stores
        the two vectorial components of the wind speed.
    time_step_min : float or "orig"
        The resolution of the temporal interpolation will be stored as part of the file name.
    vectors : boolean, optional
        If False, the absolute value of wind speed is stored instead of the two-dimensional wind
        speed vectors. Default: False
    """
    cent_lat, cent_lon = np.unique(centroids.lat), np.unique(centroids.lon)
    height, width = cent_lat.size, cent_lon.size
    wind = wind.reshape(wind.shape[0], height, width, 2)[:,::-1,:,:]
    if not vectors:
        wind = np.linalg.norm(wind, axis=-1)
        idx = np.any(wind != 0, axis=(1, 2))
    else:
        idx = np.any(wind != 0, axis=(1, 2, 3))

    coords = {
        "lat": ("lat", cent_lat),
        "lon": ("lon", cent_lon),
        "time": ("time", track.time[idx].values),
    }
    wind_var = (["time","lat","lon"], wind[idx])
    if vectors:
        wind_var[0].append("dim")
        coords["dim"] = ("dim", [0, 1])

    ds = xr.Dataset({
        "wind": wind_var,
        "eye_lat": ("time", track.lat[idx].values),
        "eye_lon": ("time", track.lon[idx].values),
    }, coords=coords, attrs={
        "crs":"epsg:4326",
        "storm_name": track.name,
        "storm_id": track.sid,
    })

    ds.wind.attrs["units"] = "m/s"
    ds.lon.attrs["units"] = "degrees_east"
    ds.lon.attrs["standard_name"] = "longitude"
    ds.lon.attrs["long_name"] = "longitude"
    ds.lon.attrs["axis"] = "X"
    ds.lat.attrs["units"] = "degrees_north"
    ds.lat.attrs["standard_name"] = "latitude"
    ds.lat.attrs["long_name"] = "latitude"
    ds.lat.attrs["axis"] = "Y"
    ds.time.attrs["standard_name"] = "time"
    ds.time.attrs["long_name"] = "time"
    ds.time.attrs["axis"] = "T"
    ds.time.encoding["dtype"] = "float64"
    if vectors:
        ds.wind.attrs["description"] = "wind speed vectors"
        ds.dim.attrs["standard_name"] = "dimension"
        ds.dim.attrs["long_name"] = "dimension"
        ds.dim.attrs["axis"] = "D"
        ds.dim.attrs["description"] = "0 = latitude, 1 = longitude"

    path_suffix = "-vectors" if vectors else ""
    fname = f"tc_winds-{get_track_name(track)}-{int(time_step_min)}min{path_suffix}.nc"
    path = base_dir / fname

    print(f"Writing to {path} ...")
    encoding = {v: {"zlib": True} for v in ds.data_vars}
    ds.to_netcdf(path=path, encoding=encoding)


def main():
    parser = argparse.ArgumentParser(
        description="Store dynamic TC wind footprints in NetCDF files.")
    parser.add_argument("--ibtracs", metavar="ID",
                        help="ID of the TC in IBTrACS.")
    parser.add_argument("--fc-name", metavar="NAME",
                        help="Name of the TC to extract from forecast file.")
    parser.add_argument("--fc-path", type=pathlib.Path, metavar="PATH",
                        help="Path to the forecast file.")
    parser.add_argument("--mit-path", type=pathlib.Path, metavar="PATH",
                        help="Path to directory containing simulated tracks (MIT).")
    parser.add_argument("--time-resolution", type=float, metavar="RESOLUTION_MINUTES", default=60,
                        help="Temporal resolution in minutes.")
    args = parser.parse_args()

    if args.ibtracs is None and (args.fc_path is None or args.fc_name is None) and args.mit_path is None:
        raise ValueError("You have to provide either an IBTrACS ID, a forecast path/name, or --mit-path.")

    centroids = setup_centroids()
    path, tracks = read_tracks(
        args.time_resolution, ibtracs_id=args.ibtracs, fc_path=args.fc_path, fc_name=args.fc_name,
        mit_path=args.mit_path)

    for track in tracks.data:
        wind = compute_wind(track, centroids)
        store_wind(path, track, centroids, wind, args.time_resolution, vectors=True)
        store_wind(path, track, centroids, wind, args.time_resolution, vectors=False)


if __name__ == "__main__":
    main()
