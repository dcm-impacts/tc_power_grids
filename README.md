
# Dynamic TC wind footprints for the modeling of power grid failures

Scripts to compute time-dependent wind footprints for historical and forecasted TCs and store in NetCDF
files for use as input in a model of power grid failures.

All wind speeds are given as 10-minute sustained winds at 10 meters above ground.


## Usage

Before executing the script, make sure that an `output` directory exists:

```shell
$ mkdir output
```

All wind footprints will be stored in NetCDF files in the `output` directory.

The scripts have the same requirements as the Python package CLIMADA, so the fastest way to set up a
working environment is by following the installation instructions of that package:
https://climada-python.readthedocs.io/en/latest/guide/Guide_Installation.html

Usage examples:

```shell
(climada_env) $ python scripts/compute_winds.py --ibtracs 2017228N14314
```

```shell
(climada_env) $ python scripts/compute_winds.py --ibtracs 2017228N14314 --time-resolution 5
```

```shell
(climada_env) $ python scripts/compute_winds.py --fc-path input/z_tigge_c_ecmf_20170824120000_ifs_glob_prod_all_glo.xml --fc-name Harvey
```

Further information can be obtained from the `python scripts/compute_winds.py --help` command line.


## Forecast data

Forecast data can be obtained from https://rda.ucar.edu/datasets/ds330.3/ with a free login.

For Hurricane Harvey we use the following file(s):

* `z_tigge_c_ecmf_20170824120000_ifs_glob_prod_all_glo.xml` (36h before landfall)

Each forecast consists of one deterministic high-resolution run, and 50 ensemble runs.


## Historical TCs considered for the Texas power grid study:

Here are the IBTrACS identifiers and officials names of the TCs:

| IBTrACS ID                                                                      | Storm name  | Year |
| ------------------------------------------------------------------------------- | ----------  | ---- |
| [2003188N11307](https://ncics.org/ibtracs/index.php?name=v04r00-2003188N11307)  | Claudette   | 2003 |
| [2007227N24269](https://ncics.org/ibtracs/index.php?name=v04r00-2007227N24269)  | Erin        | 2007 |
| [2008245N17323](https://ncics.org/ibtracs/index.php?name=v04r00-2008245N17323)  | Ike         | 2008 |
| [2010247N15266](https://ncics.org/ibtracs/index.php?name=v04r00-2010247N15266)  | Hermine     | 2010 |
| [2017228N14314](https://ncics.org/ibtracs/index.php?name=v04r00-2017228N14314)  | Harvey      | 2017 |
| [2020205N26272](https://ncics.org/ibtracs/index.php?name=v04r00-2020205N26272)  | Hanna       | 2020 |
| [2020233N14313](https://ncics.org/ibtracs/index.php?name=v04r00-2020233N14313)  | Laura       | 2020 |

In the following, I collected some reports about related outages.

* Hurricane Claudette (2003)
  - http://edition.cnn.com/2003/WEATHER/07/15/claudette/
* Hurricane Erin (2007)
  - https://www.chron.com/news/hurricanes/article/Flooding-poses-headaches-for-motorists-1805666.php
* Hurricane Ike (2008)
  - https://www.reuters.com/article/us-storm-ike-power-idUSN1532913120080916
* Hurricane Hermine (2010)
  - https://web.archive.org/web/20121009104918if_/https://www.valleycentral.com/news/story.aspx?id=508327#.UHQBPzrLeV4
  - https://web.archive.org/web/20111126061452/www.srh.noaa.gov/images/ewx/wxevent/hermine.pdf
* Hurricane Harvey (2017)
  - https://www.cbsnews.com/news/hurricane-harvey-texas-power-outages-affect-more-than-255000/
* Hurricane Hanna (2020)
  - https://www.nhc.noaa.gov/data/tcr/AL082020_Hanna.pdf
* Hurricane Laura (2020)
  - https://www.texastribune.org/2020/08/26/hurricane-laura-landfall-texas/
  - https://www.ncdc.noaa.gov/stormevents/eventdetails.jsp?id=918106
  
